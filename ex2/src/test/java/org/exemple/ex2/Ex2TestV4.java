package org.exemple.ex2;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Ex2TestV4 {

    @Test
    public void testIsLeapYearHundred() {
        int year = 300;
        boolean isLeapYear = Ex2.isLeapYearV4(year);
        Assert.assertFalse(isLeapYear);
    }

    @Test
    public void testIsLeapYearFour() {
        int year = 240;
        boolean isLeapYear = Ex2.isLeapYearV4(year);
        Assert.assertTrue(isLeapYear);
    }

    @Test
    public void testIsLeapYearNotFour() {
        int year = 363;
        boolean isLeapYear = Ex2.isLeapYearV4(year);
        Assert.assertFalse(isLeapYear);
    }

    @Test
    public void testIsLeapYearHourHundred() {
        int year = 400;
        boolean isLeapYear = Ex2.isLeapYearV4(year);
        Assert.assertTrue(isLeapYear);
    }
}