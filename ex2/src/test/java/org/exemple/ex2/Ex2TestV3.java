package org.exemple.ex2;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Ex2TestV3 {

    @Test
    public void testIsLeapYearFalseHundred() {
        int year = 300;
        boolean isLeapYear = Ex2.isLeapYearV3(year);
        Assert.assertFalse(isLeapYear);
    }

    @Test
    public void testIsLeapYearFalseFour() {
        int year = 240;
        boolean isLeapYear = Ex2.isLeapYearV3(year);
        Assert.assertTrue(isLeapYear);
    }

    @Test
    public void testIsLeapYearTrue() {
        int year = 400;
        boolean isLeapYear = Ex2.isLeapYearV3(year);
        Assert.assertTrue(isLeapYear);
    }
}