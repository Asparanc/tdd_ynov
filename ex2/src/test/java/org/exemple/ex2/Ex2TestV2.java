package org.exemple.ex2;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Ex2TestV2 {

    @Test
    public void testIsLeapYearFalse() {
        int year = 300;
        boolean isLeapYear = Ex2.isLeapYearV2(year);
        Assert.assertFalse(isLeapYear);
    }

    @Test
    public void testIsLeapYearTrue() {
        int year = 400;
        boolean isLeapYear = Ex2.isLeapYearV2(year);
        Assert.assertTrue(isLeapYear);
    }
}