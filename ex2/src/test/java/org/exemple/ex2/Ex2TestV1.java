package org.exemple.ex2;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Ex2TestV1 {

    @Test
    public void testIsLeapYearFalse() {
        int year = 2003;
        boolean isLeapYear = Ex2.isLeapYearV1(year);
        Assert.assertFalse(isLeapYear);
    }

    @Test
    public void testIsLeapYearTrue() {
        int year = 2000;
        boolean isLeapYear = Ex2.isLeapYearV1(year);
        Assert.assertTrue(isLeapYear);
    }
}