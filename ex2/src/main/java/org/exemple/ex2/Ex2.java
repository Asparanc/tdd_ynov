package org.exemple.ex2;

public class Ex2 {

    public static boolean isLeapYearV1(int year) {
        return year % 400 == 0;
    }

    public static boolean isLeapYearV2(int year) {
        return (year % 400 == 0) && (year % 100 == 0);
    }

    public static boolean isLeapYearV3(int year) {
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }

    public static boolean isLeapYearV4(int year) {
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }
}
