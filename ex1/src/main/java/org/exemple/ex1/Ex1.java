package org.exemple.ex1;

public class Ex1 {

    public static String joinStrings(String[] strings, String delimiter){
        return String.join(delimiter, strings);
    }

    public static int intAverage(int[] values) {
        int total = 0;
        int average = 0;

        if (values.length > 1) {
            for (int value : values) total += value;
            average = total / values.length;
        }

        return average;
    }
}
