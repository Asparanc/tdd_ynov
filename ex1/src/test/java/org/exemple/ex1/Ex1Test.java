package org.exemple.ex1;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Ex1Test {

    @Test
    public void testJoinStrings() {
        String joinedStrings = Ex1.joinStrings(new String[]{"Hello", "World"}, " ");
        Assert.assertEquals("Hello World", joinedStrings);
    }

    @Test
    public void testJoinStringEmptyArray() {
        String joinedStrings = Ex1.joinStrings(new String[]{}, "");
        Assert.assertEquals("", joinedStrings);
    }

    @Test
    public void testIntAverage() {
        int[] values = {2, 5, 8, 9};
        int average = Ex1.intAverage(values);
        Assert.assertEquals(6, average);
    }

    @Test
    public void testIntAverageEmptyReturnZero() {
        int[] values = {};
        int average = Ex1.intAverage(values);
        Assert.assertEquals(0, average);
    }
}