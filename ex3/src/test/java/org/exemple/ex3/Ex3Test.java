package org.exemple.ex3;


import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Ex3Test {

    public static final int MAX_LENGTH = 20;
    public static final String VALUE = "J’aimerai découper cette ligne:\nCette ligne est beaucoup trop longue alors je souhaite la découper.";

    @Test
    public void testCutLineStringDontExceedMaxLineLength() {
        String resultString = Ex3.cutLineString(VALUE, MAX_LENGTH);
        System.out.println(resultString);
        String[] lines = resultString.split("\n");
        for (String line : lines)
            Assert.assertTrue(line.length() <= MAX_LENGTH);
    }
}