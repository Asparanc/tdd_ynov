package org.exemple.ex3;

public class Ex3 {
    public static String cutLineString(String value, int lineLength) {
        StringBuilder result = new StringBuilder();
        String[] words = value.split(" ");
        int remain = lineLength;
        for (String word : words) {
            String[] newlineSplit = word.split("\n");
            if (newlineSplit.length == 1) {
                remain = getRemain(lineLength, result, remain, word);
            } else {
                int index = 0;
                for (String splittedWord : newlineSplit) {
                    remain = getRemain(lineLength, result, remain, splittedWord);
                    if (index < newlineSplit.length - 1){
                        result.append("\n");
                        remain = lineLength;
                    }
                    index ++;
                }
            }

        }
        return result.toString();
    }

    public static int getRemain(int lineLength, StringBuilder result, int remain, String word) {
        if (word.length() > remain) {
            result.append("\n");
            remain = lineLength;
        }
        result.append(word);
        if (word.length() == remain) {
            result.append("\n");
            remain = lineLength;
        }else {
            result.append(" ");
            remain -= 1;
        }
        remain -= word.length();
        return remain;
    }
}
