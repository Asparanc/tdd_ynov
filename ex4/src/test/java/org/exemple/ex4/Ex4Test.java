package org.exemple.ex4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
class Ex4Test {
    @Mock
    IHeater heater;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testStartHeaterGoodValues(){
        Mockito.when(heater.get_actual_temp()).thenReturn(35f);
        Mockito.when(heater.get_last_days_temps()).thenReturn(new float[]{15f, 25f, 40f, 18f, 36f, 28f, 14f});
        Ex4.startHeater(heater);
        Mockito.verify(heater).set_heater(Mockito.eq(true));
    }

    @Test
    public void testStartHeaterWrongActualTemp(){
        Mockito.when(heater.get_actual_temp()).thenReturn(5f);
        Mockito.when(heater.get_last_days_temps()).thenReturn(new float[]{15f, 25f, 40f, 18f, 36f, 28f, 14f});
        Ex4.startHeater(heater);
        Mockito.verify(heater).set_heater(Mockito.eq(false));
    }

    @Test
    public void testStartHeaterWrongValues(){
        Mockito.when(heater.get_actual_temp()).thenReturn(35f);
        Mockito.when(heater.get_last_days_temps()).thenReturn(new float[]{15f, 25f, 13f, 18f, 8f, 28f, 14f});
        Ex4.startHeater(heater);
        Mockito.verify(heater).set_heater(Mockito.eq(false));
    }
}