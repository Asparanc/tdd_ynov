package org.exemple.ex4;

public interface IHeater {

    public float get_actual_temp();

    public float[] get_last_days_temps();

    public void set_heater(boolean value);
}
