package org.exemple.ex4;

public class Ex4 {

    public static void startHeater(IHeater heater){
        float[] lastDaysTemp = heater.get_last_days_temps();
        float totalLastDays = 0;
        float averageLastDays;

        for (float temp : lastDaysTemp) totalLastDays += temp;
        averageLastDays = totalLastDays / lastDaysTemp.length;

        heater.set_heater(averageLastDays > 20f && heater.get_actual_temp() > 25f);
    }
}
